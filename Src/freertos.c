/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"

/* USER CODE BEGIN Includes */     
#include "cmsis_os.h"
#include "kfifo.h"
#include "irda.h"
#include "uart6_irda.h"
#include "irda_udp.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/

/* USER CODE BEGIN Variables */


osThreadId irdaTaskHandle;
osThreadId udpSendTaskHandle;
osThreadId udpRecvTaskHandle;

/* Semaphore */
osSemaphoreId irda_sem;
/* Mutex */
osMutexId fifo_mutex_rx;
osMutexId fifo_mutex_tx;
static u8 tx_buffer[USART_REC_LEN];
static u8 _tx_fifo[USART_REC_LEN];

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/

/* USER CODE BEGIN FunctionPrototypes */
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

void irda_task(void const * argument);
void udp_send_task(void const * argument);
void udp_recv_task(void const * argument);

/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* USER CODE BEGIN Application */
void MX_FREERTOS_Init(void)  
{
	
  /* add semaphores, ... */
  osSemaphoreDef(sem_rx);
  irda_sem =osSemaphoreCreate(osSemaphore(sem_rx), 1);
  
  /* add mutex, ... */
  osMutexDef(Mutex);
  fifo_mutex_rx = osMutexCreate(osMutex(Mutex));
  fifo_mutex_tx = osMutexCreate(osMutex(Mutex));
  
  /* definition and creation of defaultTask */
  osThreadDef(irdaTask, irda_task, osPriorityNormal, 0, 512);
  irdaTaskHandle = osThreadCreate(osThread(irdaTask), NULL);

 osThreadDef(udpSendtask, udp_send_task, osPriorityNormal, 0, 512);
 udpSendTaskHandle = osThreadCreate(osThread(udpSendtask), NULL);
  
 osThreadDef(udpRecvtask, udp_recv_task, osPriorityNormal, 0, 512);
 udpRecvTaskHandle = osThreadCreate(osThread(udpRecvtask), NULL);
}

/*
 * irda线程
 */
void irda_task(void const * argument)
{
	int ret;
	u16 buf_length;
	u8 *data;
	u8 *tx_fifo = _tx_fifo;
	u8 *send_data = tx_buffer;
	u16 len = 0;
	osSemaphoreWait(irda_sem, osWaitForever);
	printf("irda_task ...\n");
	//uart6_rx();
	for(;;)
	{
		osSemaphoreWait(irda_sem, osWaitForever);
		//irda收到的数据和数据的长度
		irda_get_rxbuf(&data, &len);
		//hex_dump("rxbuf data: ", data, len);
		//数据校验
		ret = recv_irda_check(data, len);
		//校验通过，取数据和数据长度存入fifo，数据不包括包头
		if (ret == 0) {
			_irda_get_rxbuf(&data, &len);
			if (len != 0) {
				//printf("irda_task len  ret = %d  %d\n", len, ret);
				write_kfifo_rx(data, len, &rxfifo);
			}
			//hex_dump("irda_task data = ", data, len);
			//检查txfifo里面有没有数据
			buf_length = 0;
			osMutexWait(fifo_mutex_tx, osWaitForever);
			len = kfifo_len(&txfifo);
			//如果txfifo中有数据
			if (len) {
				//取出txfifo的数据存入tx_fifo
				kfifo_get(&txfifo, (unsigned char *)&buf_length, 2);
				if (kfifo_get(&txfifo, tx_fifo, buf_length) != buf_length) {
					printf("irda_task kfifo_get len erro.\n");
				}
			}
			osMutexRelease(fifo_mutex_tx);
			//将txfifo中的数据打包发送
			send_irda_pack(tx_fifo, buf_length, send_data);
			//hex_dump("irda_task send_data = ", send_data, buf_length + 10);
			uart6_tx(send_data, buf_length + 10);
			osSemaphoreWait(irda_sem, osWaitForever);
		}
		uart6_rx();
	}
}

/*
 * udp接收线程
 */
void udp_recv_task(void const * argument)
{
	osDelay(100);
	if (irda_udp_init() < 0)
  	{
  		return;
  	}
	for(;;)
	{
		irda_udp_recv();
	}
}
/*
 * udp发送线程
 */
void udp_send_task(void const * argument)
{
	u16 buf_length;
	static u8 buf[USART_REC_LEN];
	u16 len;
	osDelay(100);
	printf("udp_send_task.\n");
	for(;;) {
		osMutexWait(fifo_mutex_rx, osWaitForever);
		len = kfifo_len(&rxfifo);
		if (len) {
			//如果rxfifo中有数据，取出数据发送到服务器
			kfifo_get(&rxfifo, (unsigned char *)&buf_length, 2);
			if (kfifo_get(&rxfifo, buf, buf_length) != buf_length) {
				printf("udp_send_task kfifo_get len erro.\n");
			}
			//hex_dump("recive: ", buf, buf_length);
			irda_udp_send(buf, buf_length);
			//printf("udp send ok.\n");
		}
		osMutexRelease(fifo_mutex_rx);
	}
}
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
