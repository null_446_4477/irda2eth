#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
typedef int  s32;
typedef short s16;
//typedef char  s8;

typedef unsigned int  u32;
typedef unsigned short u16;
typedef unsigned char  u8;

typedef volatile int  vs32;
typedef volatile short vs16;
//typedef volatile char  vs8;

typedef volatile unsigned int  vu32;
typedef volatile unsigned short vu16;
typedef volatile unsigned char  vu8;

#pragma arm section code = "RAMCODE"

#define enableGlobalInterrupts()   __set_PRIMASK(0);
#define disableGlobalInterrupts()  __set_PRIMASK(1);

#pragma arm section

/*
 * __enable_irq "cpsie i" : : : "memory"
 * 内在函数			操作码		PRIMASK	FAULTMASK
 * __enable_irq		CPSIE i		0
 * __disable_irq	CPSID i		1	 
 * __enable_fiq		CPSIE f	 			0
 * __disable_fiq	CPSID f	 			1
 */

#define constant_swab32(x) ((u32)(					\
	(((u32)(x) & (u32)0x000000ffUL) << 24) |		\
	(((u32)(x) & (u32)0x0000ff00UL) <<  8) |		\
	(((u32)(x) & (u32)0x00ff0000UL) >>  8) |		\
	(((u32)(x) & (u32)0xff000000UL) >> 24)))

#define jiffies HAL_GetTick()
/*
 * DEBUG: 1 错误, 2 警告, 3 基本调试信息, 4 全部调试信息
 */
#define DBG_ERR		1
#define DBG_WARN	2
#define DBG_BASE	3
#define DBG_ALL		4
#define DEBUG		g_debug
#ifdef  DEBUG
#define debug(fmt,args...)		printf (fmt ,##args)
#define debugL(level,fmt,args...)	\
	do {							\
		if (DEBUG >= level) {		\
			printf("[%07d]"fmt,(jiffies/1000),##args);		\
		}							\
	} while(0)
#else
#define debug(fmt,args...)   
#define debugL(level,fmt,args...)   
#endif

#define BUG() while (1)

#define min(x, y) (((x) < (y)) ? (x) : (y))
#define max(x, y) (((x) > (y)) ? (x) : (y))
#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((u32) &((TYPE *)0)->MEMBER)
#endif

#define LOBYTE(W)	(unsigned char)((W) & 0xFF)
#define HIBYTE(W)	(unsigned char)(((W) >> 8) & 0xFF)

#define time_diff(a, b) ((s32)(a) - (s32)(b))

#define VERSION "1.0.0.1"

void reset(void);
uint32_t HAL_GetTick(void);

extern int g_debug;

#include "cmsis_os.h"

extern osSemaphoreId debug_sem_rx;
//void reset(void);

#define MAX_IAP_DATA_SIZE 256		//一包升级包最大能传输的字节数
#define VERSION_NUM 201 // 2.00版本

#endif
