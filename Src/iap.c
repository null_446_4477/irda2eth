#include "system.h"
#include "stm32f4xx_hal.h"
#include "w25qxx.h"
#include "iap.h"
#include "crc-ccitt.h"

/*
 * iap相关操作
 */

config_net _config;
program_head _program;


void cpuId_to_macAddr(u8 *macAddr) {
	u32 CPU_ID;
	CPU_ID = *(__IO uint32_t *)(0X1FFF7A10);
	printf("chip_id = %X\r\n",CPU_ID);
	macAddr[0] = 0x52;
	macAddr[1] = 0x48;
	macAddr[2] = CPU_ID >> 24;
	macAddr[3] = CPU_ID >> 16;
	macAddr[4] = CPU_ID >> 8;
	macAddr[5] = CPU_ID;
}


/*
 * flash擦除
 * addr 为首地址
 * len为数据长度
 */
void flash_Erase(uint32_t addr, int len) {
	int i, sector;
	sector = len / 4 / 1024;
	for (i = 0; i < sector; i++) {
		SPI_FLASH_SectorErase(addr + (i * 1024 * 4));
	}
	if (len % 4096) {
		SPI_FLASH_SectorErase(addr + (i * 1024 * 4));
	}
}

/*
 * 外部flash 配置 读写
 */
void config_write(config_net *config) {
	SPI_FLASH_BufferWrite((uint8_t *)config, SPI_FLASH_CONFIG_ADDRESS, sizeof(*config));
	SPI_FLASH_WaitForWriteEnd();
}
void config_read(config_net *config) {
	SPI_FLASH_BufferRead((uint8_t *)config, SPI_FLASH_CONFIG_ADDRESS, sizeof(*config));
}


static int check_config_crc(config_net *config) {
	u16 crc;
	crc = crc_ccitt(0xffff, (const u8 *)config, offsetof(config_net, crc));
	if (config->crc != crc) {
		return -1;
	}
	return 0;
}

void read_config_net(config_net *config) {
	printf("_config_net begin...");
	config_read(config);
	//从flash里面读数据校验不通过 赋初值
	if (check_config_crc(config) < 0) {
		printf("check_config_crc < 0...");
		flash_init(config);
	}
}
void flash_init(config_net *config)
{
	u16 crc_recv = 0xffff;
	config->local_ip[0] = 192;
	config->local_ip[1] = 168;
	config->local_ip[2] = 1;
	config->local_ip[3] = 251;
	
	config->local_port = 8899;
	
	config->server_ip[0] = 192;
	config->server_ip[1] = 168;
	config->server_ip[2] = 1;
	config->server_ip[3] = 25;
	
	config->server_port = 8899;
	
	config->gateway[0] = 192;
	config->gateway[1] = 168;
	config->gateway[2] = 1;
	config->gateway[3] = 1;
	
	config->netmask[0] = 255;
	config->netmask[1] = 255;
	config->netmask[2] = 255;
	config->netmask[3] = 0;
	
	cpuId_to_macAddr(config->mac_addr);
	
	config->crc = crc_ccitt(crc_recv, (const u8 *)config, offsetof(config_net, crc));
	
	flash_Erase(SPI_FLASH_CONFIG_ADDRESS, 4 * 1024);
	config_write(config);
}

/*
 * 外部flash 程序头 读写
 */
void program_head_write(program_head *head) {
	SPI_FLASH_BufferWrite((uint8_t *)head, SPI_FLASH_PROGRAM_HEAD_ADDRESS, sizeof(*head));
	SPI_FLASH_WaitForWriteEnd();
}
void program_head_read(program_head *head) {
	SPI_FLASH_BufferRead((uint8_t *)head, SPI_FLASH_PROGRAM_HEAD_ADDRESS, sizeof(*head));
}

/*
 * 外部flash 程序 读写
 */
void program_write(char *program, int len, u32 offset) {
	SPI_FLASH_BufferWrite((uint8_t *)program, SPI_FLASH_PROGRAM_ADDRESS + offset, len);
	SPI_FLASH_WaitForWriteEnd();
}
#pragma arm section code = "RAMCODE"
void program_read(uint32_t *buffer, int len, u32 offset) {
	SPI_FLASH_BufferRead((uint8_t *)buffer, SPI_FLASH_PROGRAM_ADDRESS + offset, len);
}
#pragma arm section

void division_ip_string(char *string, u8 *ip) {
	char *result = NULL;
	int i = 0;
	result = strtok(string, ".");
	for (i = 0; i < 4; i++) {
		if (result == NULL) {
			ip[i] = 0;
			break;
		} else {
			ip[i] = atoi(result);
		}
		result = strtok(NULL, ".");
	}
}

void division_mac_string(char *string, u8 *mac) 
{    
	int ret;    
	u8 tmp[6];    
	ret = sscanf(string, "%hhx-%hhx-%hhx-%hhx-%hhx-%hhx",
		&tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5]);
	if (ret == 6) {
		memcpy(mac, tmp, 6); 
	}
}

void update_config_net(config_net *p, char *name, char *string) {
	u8 ip[4];
	u8 mac[6];
	if (!strcmp(name, "local_ip")) {
		division_ip_string(string, ip);
		memcpy(p->local_ip, ip, 4);
	} else if (!strcmp(name, "local_port")) {
		p->local_port = atoi(string);
	} else if (!strcmp(name, "server_ip")) {
		division_ip_string(string, ip);
		memcpy(p->server_ip, ip, 4);
	} else if (!strcmp(name, "server_port")) {
		p->server_port = atoi(string);
	} else if (!strcmp(name, "gateway")) {
		division_ip_string(string, ip);
		memcpy(p->gateway, ip, 4);
	} else if (!strcmp(name, "netmask")) {
		division_ip_string(string, ip);
		memcpy(p->netmask, ip, 4);
	} else if (!strcmp(name, "mac_addr")) {
		division_mac_string(string, mac);
		memcpy(p->mac_addr, mac, 6);
	}
}
void config_net_param(config_net *p, char *name, char *string) {
	u16 crc_recv = 0xffff;
	update_config_net(p, name, string);
	p->crc = crc_ccitt(crc_recv, (const u8 *)p, offsetof(config_net, crc));
	flash_Erase(SPI_FLASH_CONFIG_ADDRESS, 4 * 1024);
	config_write(p);
	printf("config_net_param ok!");
}
