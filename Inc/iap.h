#ifndef  __IAP_H
#define  __IAP_H

#include "system.h"

/*
 * 内部flash
 */
#define FLASH_START_ADDRESS   (u32)0x08000000  //开始写入的地址
#define FLASH_SECTOR_SIZE	0x80000     /* 512 * 1024 Bytes */

/*
 * 外部flash 配置
 */
#define SPI_FLASH_CONFIG_ADDRESS   (u32)0x00
#define SPI_FLASH_CONFIG_SIZE	0x10000     /* 64 * 1024 Bytes */
/*
 * 外部flash 程序
 */
#define SPI_FLASH_PROGRAM_HEAD_ADDRESS   (u32)0x10000  //程序头开始写入的地址 256 Bytes
#define SPI_FLASH_PROGRAM_ADDRESS   (u32)0x10100  //程序开始写入的地址



typedef struct _config_net {
	u8 local_ip[4];
	u16 local_port;
	u8 server_ip[4];
	u16 server_port;
	u8 gateway[4];
	u8 netmask[4];
	u8 mac_addr[6];
	u16 crc;
}config_net;

typedef struct _program_head {
	u16 version;
	char name[64];
	u32 len;
	u16 crc;
}program_head;


extern config_net _config;
extern program_head _program;

void config_write(config_net *config);
void program_head_write(program_head *program_head);
void program_head_read(program_head *head);
void program_write(char *program, int len, u32 offset);
void program_read(uint32_t *buffer, int len, u32 offset);
void program_read_64(uint64_t *buffer, int len, u16 offset);
void flash_Erase(uint32_t addr, int len);
void flash_init(config_net *config);

void read_config_net(config_net *config);
void config_net_param(config_net *p, char *name, char *string);

#endif

