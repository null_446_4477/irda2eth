#include "lwip/init.h"
#include "lwip/apps/httpd.h"
#include "lwip/debug.h"
#include "lwip/stats.h"
#include "lwip/apps/fs.h"
#include "httpd_structs.h"
#include "lwip/def.h"
#include "lwip/ip.h"
#include "lwip/tcp.h"
#include "lwip/sys.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "crc16.h"
#include "crc-ccitt.h"
#include "iap.h"
#include "stm_flash.h"


static const char* reboot_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
	int i = 0;
	for (i = 0; i < iNumParams; i++) {
		if (strcmp(pcParam[i], "reboot") == 0 && strcmp(pcValue[i], "true") == 0) {
			printf("reboot cgi...\n");
			NVIC_SystemReset();
			while (1);
		}
	}
	return NULL;
}

uint64_t _buffer(char *buf) {
	uint64_t temp = 0;;
	int i;
	int length = sizeof(buf);
	for (i = 0; i < length; i++) {
		temp |= buf[i] << i * 8;
	}
	return temp;
}

//校验写入外部flash的程序
static int check_program_crc(program_head *h) {
	u16 crc_recv = 0xffff;
	int i;
	int n;
	uint32_t writebuffer;
	n = h->len >> 2;
	n <<= 2;
	for (i = 0; i < n; i += 4) {
		// get crc
		program_read(&writebuffer, 4, i);
		crc_recv = crc_ccitt(crc_recv, (u8 *)&writebuffer, 4);
	}
	printf("check_program_crc h->len = %d  %d\n", h->len, h->len & 3);
	if (h->len & 3) {
		writebuffer = 0;
		program_read(&writebuffer, (h->len & 3), i);
		crc_recv = crc_ccitt(crc_recv, (u8 *)&writebuffer, (h->len & 3));
	}
	printf("check_program_crc  %04x\n", crc_recv);
	if (crc_recv != h->crc) {
     	printf("check_program_crc fail. %04x %04x\n", crc_recv, h->crc);
		return -1;
	}
	return 0;
}

static const char* update_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
	if (check_program_crc(&_program) < 0) {
		return "/update.html";
	}
	taskENTER_CRITICAL();
	flash_program(&_program);
	return "/index.html";	//把保存成功的信息以及修改后的信息重新上传
}

static const char* config_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
	int i;
	
	printf("index = %d  %d\n", iIndex, iNumParams);
	for (i = 0; i < iNumParams; i++) {
		printf("%s = %s\n", pcParam[i], pcValue[i]);
		config_net_param(&_config, pcParam[i],  pcValue[i]);
	}
	return "/config.html";	//把保存成功的信息以及修改后的信息重新上传
}

static const tCGI cgiURLs[] = {
	{"/update.cgi", update_cgi_handler},
	{"/config.cgi", config_cgi_handler},
	{"/reboot.cgi", reboot_cgi_handler},
};

#define NUM_CONFIG_CGI_URIS	(sizeof(cgiURLs) / sizeof(tCGI))

//CGI句柄初始化
void httpd_cgi_init(void)
{
	//配置CGI句柄LEDs control CGI) */
	http_set_cgi_handlers(cgiURLs, NUM_CONFIG_CGI_URIS);
}

