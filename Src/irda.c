#include "system.h"
#include "crc16.h"
#include "uart6_irda.h"
#include "cmsis_os.h"
#include "irda.h"

static u16 irda_head_flag = 0;
static u32 irda_head_ip = 0;

#define IRDA_IP  (((77 & 0xFF) << 24) | ((1 & 0xFF) << 16) | ((168 & 0xFF) << 8) | 192 & 0xFF)

//data为整个数据包数据 data_len为整个数据包长度
int recv_irda_check(u8 *data, u16 data_len)
{
	u16 irda_len, irda_crc, crc;
	u32 _ip = -1;
	if (data_len < 10) {
		printf("recv_irda_check data_len = %d\n", data_len);
		return -1;
	}
	irda_head_flag = (data[1] << 8) | data[0];
	/*
	_ip = (data[5] << 24) | (data[4] << 16) | (data[3] << 8) | data[2];
	if (irda_head_ip && _ip != irda_head_ip) {
		irda_head_flag = 0;
	}
	irda_head_ip = _ip;
	*/
	irda_len = (data[7] << 8) | data[6];
	if ((irda_len + 10) != data_len) {
		printf("recv_irda_check recive head len erro.\n");
		return -1;
	}
	irda_crc = (data[data_len - 2] << 8) | data[data_len - 1];
	//校验CRC
	crc = usCRC16(data, data_len - 2);
	if (crc != irda_crc) {
		printf("recv_irda_check recive head crc erro.\n");
		return -1;
	}
	return 0;
}
/* 
 * 判断fifo中是否还有存储空间
 * 如果没有存储空间，取出数据
 */
void judge_fifo_space(struct kfifo *fifo, u16 len)
{
	u16 _len, buf_length;
	static u8 _fifo[USART_REC_LEN] = {0};
	while (1) {
		_len = kfifo_avail(fifo);
		if (_len < len + 2) {
			kfifo_get(&txfifo, (unsigned char *)&buf_length, 2);
			kfifo_get(&txfifo, _fifo, buf_length);
		} else {
			break;
		}
	}
}

void write_kfifo_rx(u8 *recv_data, u16 len, struct kfifo *fifo)
{
	osMutexWait(fifo_mutex_rx, osWaitForever);
	judge_fifo_space(fifo, len);
	kfifo_put(fifo, (u8 *)&len, 2);
	kfifo_put(fifo, recv_data, len);
	osMutexRelease(fifo_mutex_rx);
}	
void write_kfifo_tx(u8 *recv_data, u16 len, struct kfifo *fifo)
{
	osMutexWait(fifo_mutex_tx, osWaitForever);
	judge_fifo_space(fifo, len);
	kfifo_put(fifo, (u8 *)&len, 2);
	kfifo_put(fifo, recv_data, len);
	osMutexRelease(fifo_mutex_tx);
}

void send_irda_pack(u8 *data, u16 data_len, u8 *buf)
{
	unsigned char *buf_crc = buf;
	unsigned short crc = 0;
	//flag
	buf_crc[0] = irda_head_flag & 0xFF;
	buf_crc[1] = (irda_head_flag >> 8) & 0xFF;
	//ip
	buf_crc[2] = IRDA_IP & 0xFF;
	buf_crc[3] = IRDA_IP >> 8 & 0xFF;
	buf_crc[4] = IRDA_IP >> 16 & 0xFF;
	buf_crc[5] = IRDA_IP >> 24 & 0xFF;
	//len
	buf_crc[6] = data_len & 0xFF;
	buf_crc[7] = (data_len >> 8) & 0xFF;
	
	memcpy(&buf_crc[8], data, data_len);
	crc = usCRC16(&buf_crc[0], data_len + 8);
	buf_crc[data_len + 8] = crc >> 8 & 0xFF;
	buf_crc[data_len + 9] = crc & 0xFF;
}

