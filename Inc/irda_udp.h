#ifndef _IRDA_UDP_H
#define _IRDA_UDP_H
#include "system.h"

int irda_udp_init(void);
void irda_udp_recv(void);
int irda_udp_send(u8 *buffer, int len);


#endif
