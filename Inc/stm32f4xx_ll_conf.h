/**
  ******************************************************************************
  * @file    stm32f4xx_ll_conf.h
  * @brief   HAL configuration file.             
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2018 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32F4xx_LL_CONF_H
#define __STM32F4xx_LL_CONF_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/* ########################## Module Selection ############################## */
/**
  * @brief This is the list of modules to be used in the HAL driver 
  */
#define LL_MODULE_ENABLED  

/* #define LL_ADC_MODULE_ENABLED   */
/* #define LL_BUS_MODULE_ENABLED   */
#define LL_CORTEX_MODULE_ENABLED
/* #define LL_CRC_MODULE_ENABLED   */
/* #define LL_DAC_MODULE_ENABLED   */
#define LL_DMA_MODULE_ENABLED
/* #define LL_DMA2D_MODULE_ENABLED   */
/* #define LL_EXTI_MODULE_ENABLED   */
/* #define LL_FMC_MODULE_ENABLED   */
/* #define LL_FSMC_MODULE_ENABLED   */
#define LL_GPIO_MODULE_ENABLED
/* #define LL_I2C_MODULE_ENABLED   */
/* #define LL_IWDG_MODULE_ENABLED   */
/* #define LL_LPTIM_MODULE_ENABLED   */
/* #define LL_PWR_MODULE_ENABLED   */
#define LL_RCC_MODULE_ENABLED
/* #define LL_RNG_MODULE_ENABLED   */
/* #define LL_RTC_MODULE_ENABLED   */
/* #define LL_SDMCC_MODULE_ENABLED   */
/* #define LL_SYSTEM_MODULE_ENABLED   */
/* #define LL_TIM_MODULE_ENABLED   */
#define LL_USART_MODULE_ENABLED
/* #define LL_USB_MODULE_ENABLED   */
/* #define LL_UTILS_MODULE_ENABLED   */
/* #define LL_WWDG_MODULE_ENABLED   */

/* Includes ------------------------------------------------------------------*/
/**
  * @brief Include module's header file 
  */

#ifdef LL_ADC_MODULE_ENABLED
  #include "stm32f4xx_ll_adc.h"
#endif

#ifdef LL_BUS_MODULE_ENABLED
  #include "stm32f4xx_ll_bus.h"
#endif

#ifdef LL_CORTEX_MODULE_ENABLED
  #include "stm32f4xx_ll_cortex.h"
#endif

#ifdef LL_CRC_MODULE_ENABLED
  #include "stm32f4xx_ll_crc.h"
#endif

#ifdef LL_DAC_MODULE_ENABLED
  #include "stm32f4xx_ll_dac.h"
#endif

#ifdef LL_DMA_MODULE_ENABLED
  #include "stm32f4xx_ll_dma.h"
#endif

#ifdef LL_DMA2D_MODULE_ENABLED
  #include "stm32f4xx_ll_dma2d.h"
#endif

#ifdef LL_EXTI_MODULE_ENABLED
  #include "stm32f4xx_ll_exti.h"
#endif

#ifdef LL_FMC_MODULE_ENABLED
  #include "stm32f4xx_ll_fmc.h"
#endif

#ifdef LL_FSMC_MODULE_ENABLED
  #include "stm32f4xx_ll_fsmc.h"
#endif

#ifdef LL_GPIO_MODULE_ENABLED
  #include "stm32f4xx_ll_gpio.h"
#endif

#ifdef LL_I2C_MODULE_ENABLED
  #include "stm32f4xx_ll_i2c.h"
#endif

#ifdef LL_IWDG_MODULE_ENABLED
  #include "stm32f4xx_ll_iwdg.h"
#endif

#ifdef LL_LPTIM_MODULE_ENABLED
  #include "stm32f4xx_ll_lptim.h"
#endif

#ifdef LL_PWR_MODULE_ENABLED
  #include "stm32f4xx_ll_pwr.h"
#endif

#ifdef LL_RCC_MODULE_ENABLED
  #include "stm32f4xx_ll_rcc.h"
#endif

#ifdef LL_RNG_MODULE_ENABLED
  #include "stm32f4xx_ll_rng.h"
#endif

#ifdef LL_RTC_MODULE_ENABLED
  #include "stm32f4xx_ll_rtc.h"
#endif

#ifdef LL_SDMCC_MODULE_ENABLED
  #include "stm32f4xx_ll_sdmcc.h"
#endif

#ifdef LL_SYSTEM_MODULE_ENABLED
  #include "stm32f4xx_ll_system.h"
#endif

#ifdef LL_TIM_MODULE_ENABLED
  #include "stm32f4xx_ll_tim.h"
#endif

#ifdef LL_USART_MODULE_ENABLED
  #include "stm32f4xx_ll_usart.h"
#endif

#ifdef LL_USB_MODULE_ENABLED
  #include "stm32f4xx_ll_usb.h"
#endif

#ifdef LL_UTILS_MODULE_ENABLED
  #include "stm32f4xx_ll_utils.h"
#endif

#ifdef LL_WWDG_MODULE_ENABLED
  #include "stm32f4xx_ll_wwdg.h"
#endif

#ifdef __cplusplus
}
#endif

#endif /* __STM32F4xx_LL_CONF_H */
 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
