#ifndef _IRDA_H
#define _IRDA_H


extern osMutexId fifo_mutex_rx;
extern osMutexId fifo_mutex_tx;
int recv_irda_check(u8 *data, u16 data_len);
void send_irda_pack(u8 *data, u16 data_len, u8 *buf);
void write_kfifo_rx(u8 *recv_data, u16 len, struct kfifo *fifo);
void write_kfifo_tx(u8 *recv_data, u16 len, struct kfifo *fifo);

#endif
