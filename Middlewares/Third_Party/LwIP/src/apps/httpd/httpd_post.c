#include "lwip/init.h"
#include "lwip/apps/httpd.h"
#include "lwip/debug.h"
#include "lwip/stats.h"
#include "lwip/apps/fs.h"
#include "httpd_structs.h"
#include "lwip/def.h"
#include "lwip/ip.h"
#include "lwip/tcp.h"
#include "lwip/sys.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "crc16.h"
#include "crc-ccitt.h"
#include "iap.h"

static const char *iap_content_type = "Content-Type: application/octet-stream";
static const char *iap_upload_uri = "/upload.cgi";
static const char *iap_update_uri = "/update.cgi";
#define POST_TITLE_FILENAME "filename=\""
struct file_upload {
	char file_name[64];
	int content_length;		// content length
	int title_done;			// title解析完成
	//char post_line[128];	// post数据行, 包含oxd oxa
	//int post_line_done;		// post数据行完整
};

static struct file_upload iap_fp;

/*
 * 输入字符串s, 去掉0d0a, 成功返回s
 */
static char* strgets(char *s, int size)
{
	int i;
	int ret = 0;
	if (!s) {
		return NULL;
	}
	for (i = 0; i < (size - 1); i++) {
		if (s[i] == '\0') {
			break;
		} else if (s[i] == 0xd && s[i + 1] == 0xa) {
			s[i] = '\0';
			s[i + 1] = '\0';
			ret = 1;
			break;
		}
	}
	return ret ? s : NULL;
}

/** Called when a POST request has been received. The application can decide
 * whether to accept it or not.
 *
 * @param connection Unique connection identifier, valid until httpd_post_end
 *        is called.
 * @param uri The HTTP header URI receiving the POST request.
 * @param http_request The raw HTTP request (the first packet, normally).
 * @param http_request_len Size of 'http_request'.
 * @param content_len Content-Length from HTTP header.
 * @param response_uri Filename of response file, to be filled when denying the
 *        request
 * @param response_uri_len Size of the 'response_uri' buffer.
 * @param post_auto_wnd Set this to 0 to let the callback code handle window
 *        updates by calling 'httpd_post_data_recved' (to throttle rx speed)
 *        default is 1 (httpd handles window updates automatically)
 * @return ERR_OK: Accept the POST request, data may be passed in
 *         another err_t: Deny the POST request, send back 'bad request'.
 */
err_t
httpd_post_begin(void *connection,
                 const char *uri,
                 const char *http_request,
                 u16_t http_request_len,
                 int content_len,
                 char *response_uri,
                 u16_t response_uri_len,
                 u8_t *post_auto_wnd)
{
	struct http_state *hs = (struct http_state*)connection;

	printf("post begin: content_len = %d\n", content_len);
	if (strcmp(uri, iap_upload_uri) == 0) {
		struct file_upload *fp = &iap_fp;
		printf("begin recv iap file... %p\n", fp);
		memset(fp, 0, sizeof(*fp));
		fp->content_length = content_len;
		hs->private_data = fp;
		strncpy(hs->post_uri, uri, sizeof(hs->post_uri) - 1);
		hs->post_uri[sizeof(hs->post_uri) - 1] = '\0';
	} else if (strcmp(uri, iap_update_uri) == 0) {
		printf("begin update...\n");
		strncpy(hs->post_uri, uri, sizeof(hs->post_uri) - 1);
		hs->post_uri[sizeof(hs->post_uri) - 1] = '\0';
	}
	return ERR_OK;
}

/** Called for each pbuf of data that has been received for a POST.
 * ATTENTION: The application is responsible for freeing the pbufs passed in!
 *
 * @param connection Unique connection identifier.
 * @param p Received data.
 * @return ERR_OK: Data accepted.
 *         another err_t: Data denied, http_post_get_response_uri will be called.
 */
err_t
httpd_post_receive_data(void *connection, struct pbuf *p)
{

	char *data;
	unsigned int len;
	err_t ret_val = ERR_ARG;

	struct http_state *hs = (struct http_state*)connection;
	if (hs != NULL && p != NULL) {
		if (strncmp(hs->post_uri, iap_upload_uri, sizeof(hs->post_uri) - 1) == 0) {
			struct file_upload *fp = (struct file_upload *)hs->private_data;
			data = p->payload;
			len = p->len;
			//hex_dump("httpd_post_receive_data: ", data, len);
			while (1) {
				if (!fp->title_done) {
					/* title parse */
					char *s = strgets(data, len);
					if (s) {
						char *sp;
						if ((sp = strstr(s, POST_TITLE_FILENAME)) != NULL) {
							strncpy(fp->file_name, sp + strlen(POST_TITLE_FILENAME),
								sizeof(fp->file_name) - 1);
							fp->file_name[sizeof(fp->file_name) - 1] = '\0';
							fp->file_name[strlen(fp->file_name) - 1] = '\0';	/* cut end '"' */
							strcpy(_program.name, fp->file_name);
							printf("post get filename: %s\n", fp->file_name);
						} else if (strncmp(s, iap_content_type, strlen(iap_content_type)) == 0) {
							printf("post title done\n");
							fp->title_done = 1;
							_program.crc = 0xffff;
							_program.len = 0;
							//flash擦除
							flash_Erase(SPI_FLASH_PROGRAM_ADDRESS, fp->content_length);
						}
						data += strlen(s) + 2;
						if (fp->title_done) {
							/* warning: not check 0d 0a */
							data += 2;
						}
					} else {
						printf("warning: post data not find 0d 0a\n");
						break;
					}
				} else {
					/* data recv */
					_program.crc = crc_ccitt(_program.crc, data, len - (data - (char *)p->payload));
					//_program.crc = us_CRC16(_program.crc, data, len - (data - (char *)p->payload));
					//下载程序
					program_write(data, len - (data - (char *)p->payload), _program.len);
					_program.len += len - (data - (char *)p->payload);
					
					printf("httpd_post_receive_data len = %d\n", _program.len);
					//hex_dump("save file: ", data, len - (data - (char *)p->payload));
					break;
				}
			}
		} /*else if (strncmp(hs->post_uri, iap_update_uri, sizeof(hs->post_uri) - 1) == 0) {
		
			//hex_dump("iap_update_uri data: ", p->payload, p->len);
			int ret = 0;
			char databuffer[256] = {0};
			while (1) {
				if ((ret * 256) < _program.len) {
					program_read(databuffer, 256, ret * 256);
					crc_recv = us_CRC16(crc_recv, databuffer, 256);
					hex_dump("databuffer data: ", databuffer, 256);
					ret ++;
				} else {
					if (crc_recv = _program.crc) {
						printf("crc_recv = _program.crc");
					} else {
						printf("crc_recv != _program.crc");
					}
					break;
				}
			}
		}
		*/
	}

	if (p != NULL) {
		pbuf_free(p);
	}

	return ERR_OK;
}

/** Called when all data is received or when the connection is closed.
 * The application must return the filename/URI of a file to send in response
 * to this POST request. If the response_uri buffer is untouched, a 404
 * response is returned.
 *
 * @param connection Unique connection identifier.
 * @param response_uri Filename of response file on success
 * @param response_uri_len Size of the 'response_uri' buffer.
 */
void
httpd_post_finished(void *connection,
                    char *response_uri,
                    u16_t response_uri_len)
{

	struct http_state *hs = (struct http_state*)connection;
	if (hs != NULL) {
		if (strncmp(hs->post_uri, iap_upload_uri, sizeof(hs->post_uri) - 1) == 0) {
			//下载程序头
			program_head_write(&_program);
			strncpy(response_uri, "/update.html", response_uri_len);
			response_uri[response_uri_len - 1] = '\0';
		} else if (strncmp(hs->post_uri, iap_update_uri, sizeof(hs->post_uri) - 1) == 0) {
			strncpy(response_uri, "/uploaddone.html", response_uri_len);
			response_uri[response_uri_len - 1] = '\0';
		}
	}

}
