#include "stm32f4xx_hal.h"
#include "main.h"
#include "w25qxx.h"

/* 宏定义 --------------------------------------------------------------------*/
//#define  SPI_FLASH_ID                       0xEF3015     //W25X16   2MB
//#define  SPI_FLASH_ID                       0xEF4015	   //W25Q16   4MB
//#define  SPI_FLASH_ID                       0XEF4017     //W25Q64   8MB
#define  SPI_FLASH_ID                       0XEF4018     //W25Q128  16MB YS-F4Pro开发默认使用

#define SPI_TIMEOUT 10

#define FLASH_SPIx SPI1

#define FLASH_SPI_CS_ENABLE() do { \
	spi1_cs_GPIO_Port->BSRR = spi1_cs_Pin << 16; \
} while (0)
#define FLASH_SPI_CS_DISABLE() do { \
	spi1_cs_GPIO_Port->BSRR = spi1_cs_Pin; \
} while (0)

#define SPI_FLASH_PageSize              256
#define SPI_FLASH_PerWritePageSize      256
#define W25X_WriteEnable		            0x06 
#define W25X_WriteDisable		            0x04 
#define W25X_ReadStatusReg		          0x05 
#define W25X_WriteStatusReg		          0x01 
#define W25X_ReadData			              0x03 
#define W25X_FastReadData		            0x0B 
#define W25X_FastReadDual		            0x3B 
#define W25X_PageProgram		            0x02 
#define W25X_BlockErase			            0xD8 
#define W25X_SectorErase		            0x20 
#define W25X_ChipErase			            0xC7 
#define W25X_PowerDown			            0xB9 
#define W25X_ReleasePowerDown	          0xAB 
#define W25X_DeviceID			              0xAB 
#define W25X_ManufactDeviceID   	      0x90 
#define W25X_JedecDeviceID		          0x9F 

#define WIP_Flag                        0x01  /* Write In Progress (WIP) flag */

#define Dummy_Byte                      0xFF

extern SPI_HandleTypeDef hspi1;

#pragma arm section code = "RAMCODE"

static HAL_StatusTypeDef _SPI_WaitFlagState(SPI_HandleTypeDef *hspi, uint32_t Flag, uint32_t State)
{
  while((((hspi->Instance->SR & Flag) == (Flag)) ? SET : RESET) != State)
  {
  }

  return HAL_OK;
}

/**
  * @brief  Transmit and Receive an amount of data in blocking mode.
  * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
  *               the configuration information for SPI module.
  * @param  pTxData pointer to transmission data buffer
  * @param  pRxData pointer to reception data buffer
  * @param  Size amount of data to be sent and received
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef _HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size)
{
  uint32_t tmp = 0U, tmp1 = 0U;
  /* Variable used to alternate Rx and Tx during transfer */
  uint32_t txallowed = 1U;
  HAL_StatusTypeDef errorcode = HAL_OK;

  /* Check Direction parameter */
  assert_param(IS_SPI_DIRECTION_2LINES(hspi->Init.Direction));

  /* Process Locked */
  __HAL_LOCK(hspi);
  
  tmp  = hspi->State;
  tmp1 = hspi->Init.Mode;
  
  if(!((tmp == HAL_SPI_STATE_READY) || \
    ((tmp1 == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_2LINES) && (tmp == HAL_SPI_STATE_BUSY_RX))))
  {
    errorcode = HAL_BUSY;
    goto error;
  }

  if((pTxData == NULL) || (pRxData == NULL) || (Size == 0))
  {
    errorcode = HAL_ERROR;
    goto error;
  }

  /* Don't overwrite in case of HAL_SPI_STATE_BUSY_RX */
  if(hspi->State == HAL_SPI_STATE_READY)
  {
    hspi->State = HAL_SPI_STATE_BUSY_TX_RX;
  }

  /* Set the transaction information */
  hspi->ErrorCode   = HAL_SPI_ERROR_NONE;
  hspi->pRxBuffPtr  = (uint8_t *)pRxData;
  hspi->RxXferCount = Size;
  hspi->RxXferSize  = Size;
  hspi->pTxBuffPtr  = (uint8_t *)pTxData;
  hspi->TxXferCount = Size;
  hspi->TxXferSize  = Size;

  /*Init field not used in handle to zero */
  hspi->RxISR       = NULL;
  hspi->TxISR       = NULL;

  /* Check if the SPI is already enabled */
  if((hspi->Instance->CR1 &SPI_CR1_SPE) != SPI_CR1_SPE)
  {
    /* Enable SPI peripheral */
    __HAL_SPI_ENABLE(hspi);
  }
  {
    if((hspi->Init.Mode == SPI_MODE_SLAVE) || (hspi->TxXferCount == 0x01U))
    {
      *((__IO uint8_t*)&hspi->Instance->DR) = (*pTxData);
      pTxData += sizeof(uint8_t);
      hspi->TxXferCount--;
    }
    while((hspi->TxXferCount > 0U) || (hspi->RxXferCount > 0U))
    {
      /* check TXE flag */
      if(txallowed && (hspi->TxXferCount > 0U) && (__HAL_SPI_GET_FLAG(hspi, SPI_FLAG_TXE)))
      {
        *(__IO uint8_t *)&hspi->Instance->DR = (*pTxData++);
        hspi->TxXferCount--;
        /* Next Data is a reception (Rx). Tx not allowed */ 
        txallowed = 0U;

      }

      /* Wait until RXNE flag is reset */
      if((hspi->RxXferCount > 0U) && (__HAL_SPI_GET_FLAG(hspi, SPI_FLAG_RXNE)))
      {
        (*(uint8_t *)pRxData++) = hspi->Instance->DR;
        hspi->RxXferCount--;
        /* Next Data is a Transmission (Tx). Tx is allowed */ 
        txallowed = 1U;
      }
    }
  }

  /* Wait until TXE flag */
  _SPI_WaitFlagState(hspi, SPI_FLAG_TXE, SET);
  
  /* Check Busy flag */
  _SPI_WaitFlagState(hspi, SPI_FLAG_BSY, RESET);
  
  /* Clear overrun flag in 2 Lines communication mode because received is not read */
  if(hspi->Init.Direction == SPI_DIRECTION_2LINES)
  {
    __HAL_SPI_CLEAR_OVRFLAG(hspi);
  }

error :
  hspi->State = HAL_SPI_STATE_READY;
  __HAL_UNLOCK(hspi);
  return errorcode;
}

/**
  * 函数功能: 往串行Flash读取写入一个字节数据并接收一个字节数据
  * 输入参数: byte：待发送数据
  * 返 回 值: uint8_t：接收到的数据
  * 说    明：无
  */
static uint8_t SPI_FLASH_SendByte(uint8_t byte)
{
	uint8_t d_read, d_send = byte;
	if (_HAL_SPI_TransmitReceive(&hspi1, &d_send, &d_read, 1) != HAL_OK)
		d_read = Dummy_Byte;

	return d_read;
}
#pragma arm section

/**
  * 函数功能: 使能串行Flash写操作
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明：无
  */
static void SPI_FLASH_WriteEnable(void)
{
	/* 选择串行FLASH: CS低电平 */
	FLASH_SPI_CS_ENABLE();

	/* 发送命令：写使能 */
	SPI_FLASH_SendByte(W25X_WriteEnable);

	/* 禁用串行Flash：CS高电平 */
	FLASH_SPI_CS_DISABLE();
}

/**
  * 函数功能: 擦除扇区
  * 输入参数: SectorAddr：待擦除扇区地址，要求为4096倍数
  * 返 回 值: 无
  * 说    明：串行Flash最小擦除块大小为4KB(4096字节)，即一个扇区大小，要求输入参数
  *           为4096倍数。在往串行Flash芯片写入数据之前要求先擦除空间。
  */
void SPI_FLASH_SectorErase(uint32_t SectorAddr)
{
	/* 发送FLASH写使能命令 */
	SPI_FLASH_WriteEnable();
	SPI_FLASH_WaitForWriteEnd();
	/* 擦除扇区 */
	/* 选择串行FLASH: CS低电平 */
	FLASH_SPI_CS_ENABLE();
	/* 发送扇区擦除指令*/
	SPI_FLASH_SendByte(W25X_SectorErase);
	/*发送擦除扇区地址的高位*/
	SPI_FLASH_SendByte((SectorAddr & 0xFF0000) >> 16);
	/* 发送擦除扇区地址的中位 */
	SPI_FLASH_SendByte((SectorAddr & 0xFF00) >> 8);
	/* 发送擦除扇区地址的低位 */
	SPI_FLASH_SendByte(SectorAddr & 0xFF);
	/* 禁用串行FLASH: CS 高电平 */
	FLASH_SPI_CS_DISABLE();
	/* 等待擦除完毕*/
	SPI_FLASH_WaitForWriteEnd();
}

/**
  * 函数功能: 擦除整片
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明：擦除串行Flash整片空间
  */
void SPI_FLASH_BulkErase(void)
{
	/* 发送FLASH写使能命令 */
	SPI_FLASH_WriteEnable();

	/* 整片擦除 Erase */
	/* 选择串行FLASH: CS低电平 */
	FLASH_SPI_CS_ENABLE();
	/* 发送整片擦除指令*/
	SPI_FLASH_SendByte(W25X_ChipErase);
	/* 禁用串行FLASH: CS高电平 */
	FLASH_SPI_CS_DISABLE();

	/* 等待擦除完毕*/
	SPI_FLASH_WaitForWriteEnd();
}

/**
  * 函数功能: 往串行FLASH按页写入数据，调用本函数写入数据前需要先擦除扇区
  * 输入参数: pBuffer：待写入数据的指针
  *           WriteAddr：写入地址
  *           NumByteToWrite：写入数据长度，必须小于等于SPI_FLASH_PerWritePageSize
  * 返 回 值: 无
  * 说    明：串行Flash每页大小为256个字节
  */
void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	/* 发送FLASH写使能命令 */
	SPI_FLASH_WriteEnable();

	/* 寻找串行FLASH: CS低电平 */
	FLASH_SPI_CS_ENABLE();
	/* 写送写指令*/
	SPI_FLASH_SendByte(W25X_PageProgram);
	/*发送写地址的高位*/
	SPI_FLASH_SendByte((WriteAddr & 0xFF0000) >> 16);
	/*发送写地址的中位*/
	SPI_FLASH_SendByte((WriteAddr & 0xFF00) >> 8);
	/*发送写地址的低位*/
	SPI_FLASH_SendByte(WriteAddr & 0xFF);

	if (NumByteToWrite > SPI_FLASH_PerWritePageSize) {
		NumByteToWrite = SPI_FLASH_PerWritePageSize;
		//printf("Err: SPI_FLASH_PageWrite too large!\n");
	}

	/* 写入数据*/
	while (NumByteToWrite--) {
		/* 发送当前要写入的字节数据 */
		SPI_FLASH_SendByte(*pBuffer);
		/* 指向下一字节数据 */
		pBuffer++;
	}

	/* 禁用串行FLASH: CS 高电平 */
	FLASH_SPI_CS_DISABLE();

	/* 等待写入完毕*/
	SPI_FLASH_WaitForWriteEnd();
}

/**
  * 函数功能: 往串行FLASH写入数据，调用本函数写入数据前需要先擦除扇区
  * 输入参数: pBuffer：待写入数据的指针
  *           WriteAddr：写入地址
  *           NumByteToWrite：写入数据长度
  * 返 回 值: 无
  * 说    明：该函数可以设置任意写入数据长度
  */
void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite)
{
	uint32_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;

	Addr = WriteAddr % SPI_FLASH_PageSize;
	count = SPI_FLASH_PageSize - Addr;
	NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
	NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

	if (Addr == 0) { /* 若地址与 SPI_FLASH_PageSize 对齐  */
		if (NumOfPage == 0) { /* NumByteToWrite < SPI_FLASH_PageSize */
			SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
		} else { /* NumByteToWrite > SPI_FLASH_PageSize */
			while (NumOfPage--) {
				SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
				WriteAddr +=  SPI_FLASH_PageSize;
				pBuffer += SPI_FLASH_PageSize;
			}

			SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
		}
	} else { /* 若地址与 SPI_FLASH_PageSize 不对齐 */
		if (NumOfPage == 0) { /* NumByteToWrite < SPI_FLASH_PageSize */
			if (NumOfSingle > count) { /* (NumByteToWrite + WriteAddr) > SPI_FLASH_PageSize */
				temp = NumOfSingle - count;

				SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
				WriteAddr +=  count;
				pBuffer += count;

				SPI_FLASH_PageWrite(pBuffer, WriteAddr, temp);
			} else {
				SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
			}
		} else { /* NumByteToWrite > SPI_FLASH_PageSize */
			NumByteToWrite -= count;
			NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
			NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

			SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
			WriteAddr +=  count;
			pBuffer += count;

			while (NumOfPage--) {
				SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
				WriteAddr +=  SPI_FLASH_PageSize;
				pBuffer += SPI_FLASH_PageSize;
			}

			if (NumOfSingle != 0) {
				SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
			}
		}
	}
}

#pragma arm section code = "RAMCODE"
/**
  * 函数功能: 从串行Flash读取数据
  * 输入参数: pBuffer：存放读取到数据的指针
  *           ReadAddr：读取数据目标地址
  *           NumByteToRead：读取数据长度
  * 返 回 值: 无
  * 说    明：该函数可以设置任意读取数据长度
  */
void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead)
{
	/* 选择串行FLASH: CS低电平 */
	FLASH_SPI_CS_ENABLE();

	/* 发送 读 指令 */
	SPI_FLASH_SendByte(W25X_ReadData);

	/* 发送 读 地址高位 */
	SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
	/* 发送 读 地址中位 */
	SPI_FLASH_SendByte((ReadAddr & 0xFF00) >> 8);
	/* 发送 读 地址低位 */
	SPI_FLASH_SendByte(ReadAddr & 0xFF);

	while (NumByteToRead--) { /* 读取数据 */
		/* 读取一个字节*/
		*pBuffer = SPI_FLASH_SendByte(Dummy_Byte);
		/* 指向下一个字节缓冲区 */
		pBuffer++;
	}

	/* 禁用串行FLASH: CS 高电平 */
	FLASH_SPI_CS_DISABLE();
}
#pragma arm section

/**
  * 函数功能: 读取串行Flash型号的ID
  * 输入参数: 无
  * 返 回 值: uint32_t：串行Flash的型号ID
  * 说    明：  FLASH_ID      IC型号      存储空间大小
                0xEF3015      W25X16        2M byte
                0xEF4015	    W25Q16        4M byte
                0XEF4017      W25Q64        8M byte
                0XEF4018      W25Q128       16M byte  (YS-F1Pro开发板默认配置)
  */
uint32_t SPI_FLASH_ReadID(void)
{
	uint32_t Temp = 0, Temp0 = 0, Temp1 = 0, Temp2 = 0;

	/* 选择串行FLASH: CS低电平 */
	FLASH_SPI_CS_ENABLE();

	/* 发送命令：读取芯片型号ID */
	SPI_FLASH_SendByte(W25X_JedecDeviceID);

	/* 从串行Flash读取一个字节数据 */
	Temp0 = SPI_FLASH_SendByte(Dummy_Byte);

	/* 从串行Flash读取一个字节数据 */
	Temp1 = SPI_FLASH_SendByte(Dummy_Byte);

	/* 从串行Flash读取一个字节数据 */
	Temp2 = SPI_FLASH_SendByte(Dummy_Byte);

	/* 禁用串行Flash：CS高电平 */
	FLASH_SPI_CS_DISABLE();

	Temp = (Temp0 << 16) | (Temp1 << 8) | Temp2;
	return Temp;
}

/**
  * 函数功能: 读取串行Flash设备ID
  * 输入参数: 无
  * 返 回 值: uint32_t：串行Flash的设备ID
  * 说    明：
  */
uint32_t SPI_FLASH_ReadDeviceID(void)
{
	uint32_t Temp = 0;

	/* 选择串行FLASH: CS低电平 */
	FLASH_SPI_CS_ENABLE();

	/* 发送命令：读取芯片设备ID * */
	SPI_FLASH_SendByte(W25X_DeviceID);
	SPI_FLASH_SendByte(Dummy_Byte);
	SPI_FLASH_SendByte(Dummy_Byte);
	SPI_FLASH_SendByte(Dummy_Byte);

	/* 从串行Flash读取一个字节数据 */
	Temp = SPI_FLASH_SendByte(Dummy_Byte);

	/* 禁用串行Flash：CS高电平 */
	FLASH_SPI_CS_DISABLE();

	return Temp;
}

/**
  * 函数功能: 启动连续读取数据串
  * 输入参数: ReadAddr：读取地址
  * 返 回 值: 无
  * 说    明：Initiates a read data byte (READ) sequence from the Flash.
  *           This is done by driving the /CS line low to select the device,
  *           then the READ instruction is transmitted followed by 3 bytes
  *           address. This function exit and keep the /CS line low, so the
  *           Flash still being selected. With this technique the whole
  *           content of the Flash is read with a single READ instruction.
  */
void SPI_FLASH_StartReadSequence(uint32_t ReadAddr)
{
	/* Select the FLASH: Chip Select low */
	FLASH_SPI_CS_ENABLE();

	/* Send "Read from Memory " instruction */
	SPI_FLASH_SendByte(W25X_ReadData);

	/* Send the 24-bit address of the address to read from -----------------------*/
	/* Send ReadAddr high nibble address byte */
	SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
	/* Send ReadAddr medium nibble address byte */
	SPI_FLASH_SendByte((ReadAddr & 0xFF00) >> 8);
	/* Send ReadAddr low nibble address byte */
	SPI_FLASH_SendByte(ReadAddr & 0xFF);
}

/**
  * 函数功能: 等待数据写入完成
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明：Polls the status of the Write In Progress (WIP) flag in the
  *           FLASH's status  register  and  loop  until write  opertaion
  *           has completed.
  */
void SPI_FLASH_WaitForWriteEnd(void)
{
	uint8_t FLASH_Status = 0;

	/* Select the FLASH: Chip Select low */
	FLASH_SPI_CS_ENABLE();

	/* Send "Read Status Register" instruction */
	SPI_FLASH_SendByte(W25X_ReadStatusReg);

	/* Loop as long as the memory is busy with a write cycle */
	do {
		/* Send a dummy byte to generate the clock needed by the FLASH
		and put the value of the status register in FLASH_Status variable */
		FLASH_Status = SPI_FLASH_SendByte(Dummy_Byte);
	} while ((FLASH_Status & WIP_Flag) == SET); /* Write in progress */

	/* Deselect the FLASH: Chip Select high */
	FLASH_SPI_CS_DISABLE();
}


/**
  * 函数功能: 进入掉电模式
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明：无
  */
void SPI_Flash_PowerDown(void)
{
	/* Select the FLASH: Chip Select low */
	FLASH_SPI_CS_ENABLE();

	/* Send "Power Down" instruction */
	SPI_FLASH_SendByte(W25X_PowerDown);

	/* Deselect the FLASH: Chip Select high */
	FLASH_SPI_CS_DISABLE();
}

/**
  * 函数功能: 唤醒串行Flash
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明：无
  */
void SPI_Flash_WAKEUP(void)
{
	/* Select the FLASH: Chip Select low */
	FLASH_SPI_CS_ENABLE();

	/* Send "Power Down" instruction */
	SPI_FLASH_SendByte(W25X_ReleasePowerDown);

	/* Deselect the FLASH: Chip Select high */
	FLASH_SPI_CS_DISABLE();
}


