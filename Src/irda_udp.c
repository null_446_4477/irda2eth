#include <lwip/sockets.h> 
#include "system.h"
#include "irda_udp.h"
#include "uart6_irda.h"
#include "irda.h"
#include "iap.h"

#define PORT 8899 
#define SERVER_IP "192.168.1.25"
static int irda_sock_fd = -1;
static struct sockaddr_in ra;
int addr_flag = 0;
static socklen_t ralen;

void set_ra()
{
	memset(&ra, 0, sizeof(struct sockaddr_in));
	ra.sin_family = AF_INET;
	//ra.sin_addr.s_addr = inet_addr(SERVER_IP);
	//ra.sin_port = htons(PORT);
	ra.sin_addr.s_addr = inet_addr(_config.server_ip);
	ra.sin_port = htons(_config.server_port);
}
int irda_udp_init(void)
{
	struct sockaddr_in sa;
	irda_sock_fd = socket(PF_INET, SOCK_DGRAM, 0);

	if ( irda_sock_fd < 0 )
	{
		printf("socket call failed");
		return -1;
	}
	memset(&sa, 0, sizeof(struct sockaddr_in));
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	//sa.sin_port = htons(PORT);
	sa.sin_port = htons(_config.local_port);

	if (bind(irda_sock_fd, (struct sockaddr *)&sa, sizeof(struct sockaddr_in)) == -1)
	{
		close(irda_sock_fd);
		return -1;
	}
	return 0;
}

void irda_udp_recv(void)
{
	int recv_data; 
	static char data_buffer[USART_REC_LEN] = {0};
	ralen = sizeof(struct sockaddr_in);
	recv_data = recvfrom(irda_sock_fd, data_buffer,sizeof(data_buffer), 0, (struct sockaddr*)&ra, &ralen);
	if(recv_data > 0)
	{
		write_kfifo_tx((u8 *)data_buffer, recv_data, &txfifo);
		//hex_dump("irda_udp_recv ", (unsigned char *)data_buffer, recv_data);
	} 
}

int irda_udp_send(u8 *buffer, int len)
{
	int sent_data;
	set_ra();
	sent_data = sendto(irda_sock_fd, buffer, len, 0, (struct sockaddr*)&ra, sizeof(ra));
	if(sent_data < 0)
	{
		printf("send failed\n");
		return -1;
	} 
	return 0;
}
