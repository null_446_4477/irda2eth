#ifndef _UART_IRDA_H
#define _UART_IRDA_H

#include "system.h"
#include "kfifo.h"

#define USART_REC_LEN 1500

typedef struct _uart_param {
	u32 baud;
	u8 stop_bit;
	u8 data_bit;
	u8 parity_bit;
} __attribute__ ((packed)) uart_param;

extern osSemaphoreId irda_sem;
extern struct kfifo txfifo;
extern struct kfifo rxfifo;

void irda_get_rxbuf(u8 **p, u16 *len);
void _irda_get_rxbuf(u8 **p, u16 *len);
void irda_init(void);
int uart6_tx(u8 *buf, u16 len);
int uart6_rx(void);
void hex_dump(const char *str, unsigned char *pSrcBufVA, unsigned int SrcBufLen);
#endif
