#include "stm32f4xx_ll_conf.h"
#include "stm32f4xx_hal_conf.h"
#include "uart6_irda.h"
#include "irda.h"


#define USART USART6
#define USART_REC_LEN 1500

struct kfifo txfifo;
struct kfifo rxfifo;

static u8 fifo_rxbuf[1024 * 4];
static u8 fifo_txbuf[8 * 1024];

/* rx/tx buffer, used by DMA */
static u8 dma_rxbuf[USART_REC_LEN];
static u8 dma_txbuf[USART_REC_LEN];
static u32 rx_len;
static uart_param irda = {115200, 1, 8, 1};

void get_sys_clk(void)
{
	LL_RCC_ClocksTypeDef sys_clk;
	LL_RCC_GetSystemClocksFreq(&sys_clk);
}

static void _nvic_init()
{
	/*
	 * Priority不能高于 configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY 
	 * 如果DMA优先级高于UART6，进中断后关闭DMA时会发生DMA_TC中断
	 * 此时产生中断嵌套，导致数据接收不成功
	 */
	HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 10, 0);
	HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 10, 0);
	HAL_NVIC_SetPriority(USART6_IRQn, 10, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);
	HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
	HAL_NVIC_EnableIRQ(USART6_IRQn);
	return;
}

/*
 * uart DMA发送
 */
static int _dma_tx(u8 *buf, u16 len)
{
	LL_DMA_InitTypeDef dma;
	memset(&dma, 0, sizeof(LL_DMA_InitTypeDef));
	// 设置DMA通道
	dma.Channel = LL_DMA_CHANNEL_5;
	// 设置寄存器地址
	dma.PeriphOrM2MSrcAddress = (u32)(&USART->DR);
	// 设置内存地址
	dma.MemoryOrM2MDstAddress = (u32)(buf);
	// 设置传输方向
	dma.Direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH;
	// 设置缓冲长度
	dma.NbData = len;
	// 设置外设地址递增
	dma.PeriphOrM2MSrcIncMode = LL_DMA_PERIPH_NOINCREMENT;
	// 设置内存地址递增
	dma.MemoryOrM2MDstIncMode = LL_DMA_MEMORY_INCREMENT;
	// 设置外设数据字长
	dma.PeriphOrM2MSrcDataSize = LL_DMA_PDATAALIGN_BYTE;
	// 设置内存数据字长
	dma.MemoryOrM2MDstDataSize = LL_DMA_MDATAALIGN_BYTE;
	// DMA模式
	dma.Mode = LL_DMA_MODE_NORMAL;
	// 设置优先级
	dma.Priority = LL_DMA_PRIORITY_MEDIUM;
	// 设置是否使用FIFO
	dma.FIFOMode = LL_DMA_FIFOMODE_DISABLE;
	// 设置FIFO阈值
	dma.FIFOThreshold = LL_DMA_FIFOTHRESHOLD_1_4;
	// 
	dma.MemBurst = LL_DMA_MBURST_SINGLE;
	//
	dma.PeriphBurst = LL_DMA_PBURST_SINGLE;
	// 初始化DMA
	LL_DMA_Init(DMA2, LL_DMA_STREAM_7, &dma);
	// 关闭双缓冲模式
	LL_DMA_DisableDoubleBufferMode(DMA2, LL_DMA_STREAM_7);
	// 设置DMA中断模式
	LL_DMA_EnableIT_TC(DMA2, LL_DMA_STREAM_7);
	// 设置长度计数
	LL_DMA_SetDataLength(DMA2, LL_DMA_STREAM_7, len);
	// 打开DMA
	LL_DMA_EnableStream(DMA2, LL_DMA_STREAM_7);
	return 0;
}

/*
 * UART DMA RX start
 */
static void _dma_rx()
{
	LL_DMA_InitTypeDef dma;
	u16 len = sizeof(dma_rxbuf);
	memset(&dma, 0, sizeof(LL_DMA_InitTypeDef));
	// 设置DMA通道
	dma.Channel = LL_DMA_CHANNEL_5;
	// 设置寄存器地址
	dma.PeriphOrM2MSrcAddress = (u32)(&USART->DR);
	// 设置内存地址
	dma.MemoryOrM2MDstAddress = (u32)(dma_rxbuf);
	// 设置传输方向
	dma.Direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY;
	// 设置缓冲长度
	dma.NbData = len;
	// 设置外设地址递增
	dma.PeriphOrM2MSrcIncMode = LL_DMA_PERIPH_NOINCREMENT;
	// 设置内存地址递增
	dma.MemoryOrM2MDstIncMode = LL_DMA_MEMORY_INCREMENT;
	// 设置外设数据字长
	dma.PeriphOrM2MSrcDataSize = LL_DMA_PDATAALIGN_BYTE;
	// 设置内存数据字长
	dma.MemoryOrM2MDstDataSize = LL_DMA_MDATAALIGN_BYTE;
	// DMA模式
	dma.Mode = LL_DMA_MODE_NORMAL;
	// 设置优先级
	dma.Priority = LL_DMA_PRIORITY_VERYHIGH;
	// 设置是否使用FIFO
	dma.FIFOMode = LL_DMA_FIFOMODE_DISABLE;
	// 设置FIFO阈值
	dma.FIFOThreshold = LL_DMA_FIFOTHRESHOLD_1_4;
	// 
	dma.MemBurst = LL_DMA_MBURST_SINGLE;
	//
	dma.PeriphBurst = LL_DMA_PBURST_SINGLE;
	// 初始化DMA
	LL_DMA_Init(DMA2, LL_DMA_STREAM_2, &dma);
	// 关闭双缓冲模式
	LL_DMA_DisableDoubleBufferMode(DMA2, LL_DMA_STREAM_2);
	// 设置DMA中断模式
	LL_DMA_EnableIT_TC(DMA2, LL_DMA_STREAM_2);
	// 设置长度计数
	LL_DMA_SetDataLength(DMA2, LL_DMA_STREAM_2, len);
	LL_DMA_ClearFlag_TE2(DMA2);
	LL_DMA_ClearFlag_TC2(DMA2);
	// 打开DMA
	LL_DMA_EnableStream(DMA2, LL_DMA_STREAM_2);
	
	// uart rx enable
	//USART->CR1 |= USART_Mode_Rx;
	/* Uart Interrupt config, enable idle intr */
	LL_USART_EnableIT_IDLE(USART);
	return;
}
/*
 * UART 发送函数
 */
int uart6_tx(u8 *buf, u16 len)
{
	len = min(sizeof(dma_txbuf), len);
	
	memcpy(dma_txbuf, buf, len);
	
	_dma_tx(dma_txbuf, len);
	return 0;
}

/*
 * uart2 开启接收
 */
int uart6_rx()
{
	_dma_rx();
	return 0;
}
void irda_get_rxbuf(u8 **p, u16 *len)
{
	*p = dma_rxbuf;
	*len = rx_len;
	//memcpy(len, dma_rxbuf + 6, 2);
}
void _irda_get_rxbuf(u8 **p, u16 *len)
{
	*p = dma_rxbuf + 8;
	memcpy(len, dma_rxbuf + 6, 2);
}
void uart6_init(uart_param *param)
{
	/* gpio config */
	LL_GPIO_InitTypeDef GPIO_InitStructure;
	LL_USART_InitTypeDef USART_InitStructure;

	kfifo_init(&rxfifo, fifo_rxbuf, sizeof(fifo_rxbuf));
	kfifo_init(&txfifo, fifo_txbuf, sizeof(fifo_txbuf));
	
	_nvic_init();
	/* Enable the GPIOs clocks */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	/* Enable the UART6 clocks */
	__HAL_RCC_USART6_CLK_ENABLE();
	/* Enable the DMA2 clocks */
	__HAL_RCC_DMA2_CLK_ENABLE();

	/* Configure uart6_tx as alternate function push-pull */
	GPIO_InitStructure.Pin = LL_GPIO_PIN_6;
	GPIO_InitStructure.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;	// wjzhe, 10M to 2M
	GPIO_InitStructure.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStructure.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStructure.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	/* Configure usat6_rx as input floating */
	GPIO_InitStructure.Pin = LL_GPIO_PIN_7;
	GPIO_InitStructure.Mode = LL_GPIO_MODE_ALTERNATE;
	LL_GPIO_Init(GPIOC, &GPIO_InitStructure);

	LL_GPIO_SetAFPin_0_7(GPIOC, LL_GPIO_PIN_6, LL_GPIO_AF_8);
	LL_GPIO_SetAFPin_0_7(GPIOC, LL_GPIO_PIN_7, LL_GPIO_AF_8);
	
	/* USART2 configured as follow:
		- BaudRate = 115200 baud
		- Word Length = 8 Bits
		- One Stop Bit
		- No parity
		- Hardware flow control disabled (RTS and CTS signals)
		- Receive and transmit enabled
	*/
	memset(&USART_InitStructure, 0, sizeof(USART_InitStructure));
	USART_InitStructure.BaudRate = param->baud;;
	USART_InitStructure.DataWidth = LL_USART_DATAWIDTH_8B; 
	USART_InitStructure.StopBits = LL_USART_STOPBITS_1; 
	USART_InitStructure.Parity = LL_USART_PARITY_NONE; 
	USART_InitStructure.HardwareFlowControl = LL_USART_HWCONTROL_NONE; 
	USART_InitStructure.TransferDirection =  LL_USART_DIRECTION_TX_RX; 
	
	/* Configure the USART1*/ 
	LL_USART_Init(USART, &USART_InitStructure);

	// Deinitializes the DMA2 Channel
	LL_DMA_DeInit(DMA2, LL_DMA_STREAM_2);
	LL_DMA_DeInit(DMA2, LL_DMA_STREAM_7);

	// DMA发送UART数据
	LL_USART_EnableDMAReq_TX(USART);

	// DMA接收UART数据
	LL_USART_EnableDMAReq_RX(USART);

	// 开启接收dma rx
	_dma_rx();

	/* Set the USART6 prescaler */
    LL_USART_SetIrdaPrescaler(USART, 0x1);
	/* Configure the USART6 IrDA mode */
	LL_USART_ConfigIrdaMode(USART);
    /* Enable the USART6 IrDA mode */
	LL_USART_EnableIrda(USART);
	
	/* Enable the USART */
	LL_USART_Enable(USART);
}
void irda_init(void)
{
	get_sys_clk();
	uart6_init(&irda);
}



// DMA发送完成中断
void DMA2_Stream7_IRQHandler(void)
{
	LL_DMA_ClearFlag_TC7(DMA2);
	LL_DMA_DisableStream(DMA2, LL_DMA_STREAM_7);
	LL_USART_EnableIT_TC(USART);
	return;
}

// DMA接收完成中断
void DMA2_Stream2_IRQHandler(void)
{
	LL_DMA_ClearFlag_TC2(DMA2);
	LL_DMA_DisableStream(DMA2, LL_DMA_STREAM_2);
	LL_DMA_SetDataLength(DMA2, LL_DMA_STREAM_2, sizeof(dma_rxbuf));
	LL_DMA_EnableStream(DMA2, LL_DMA_STREAM_2);
	return;
}

// 空闲接收中断
void USART6_IRQHandler(void)
{
	u8 ReadData;
	u32 sr = USART->SR;
	
	// 空闲中断
	if (sr & LL_USART_SR_IDLE) {
		LL_DMA_DisableIT_TC(DMA2, LL_DMA_STREAM_2);
		LL_DMA_DisableStream(DMA2, LL_DMA_STREAM_2);
		LL_USART_DisableIT_IDLE(USART);

		ReadData = USART->DR;
		rx_len = sizeof(dma_rxbuf) - LL_DMA_GetDataLength(DMA2, LL_DMA_STREAM_2);
		osSemaphoreRelease(irda_sem);
	}
	// rx
	while (sr & LL_USART_SR_RXNE) {
		/* can not reach here */
		ReadData = USART->DR;
		sr = USART->SR;
	}
	// over err
	if (sr & LL_USART_SR_ORE) {
		ReadData = USART->DR;
	}
	// NE
	if (sr & LL_USART_SR_NE) {
		ReadData = USART->DR;
	}
	// FE
	if (sr & LL_USART_SR_FE) {
		ReadData = USART->DR;
	}
	// PE
	if (sr & LL_USART_SR_PE) {
		ReadData = USART->DR;
	}
	// send complete
	if (sr & LL_USART_SR_TC) {
		// 不可以用sr = 0清TC，会出现TC中断不正常情况
		LL_USART_DisableIT_TC(USART);
		if (!(sr & LL_USART_SR_IDLE)) {
			osSemaphoreRelease(irda_sem);
		}
	}
	return;
}


/* USER CODE BEGIN 4 */

void hex_dump(const char *str, unsigned char *pSrcBufVA, unsigned int SrcBufLen)
{
	const unsigned char *pt;
	int x;

	pt = pSrcBufVA;
	printf("%s: %p, len = %d\n", str, pSrcBufVA, SrcBufLen);
	for (x = 0; x < SrcBufLen; x++) {
		if (x % 16 == 0) {
			printf("0x%04x : ", x);
		}
		printf("%02x ", ((unsigned char)pt[x]));
		if ((x & 0xF) == 15) {
			printf("\n");	// printf("%s\n", buf);
		}
	}
	x &= 0xF;
	printf("\n");
}

