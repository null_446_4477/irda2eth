#ifndef _LINUX_CRC_CCITT_H
#define _LINUX_CRC_CCITT_H

#include "system.h"
#include <stddef.h>
//#include "crc-types.h"

extern const u16 crc_ccitt_table[256];

extern u16 crc_ccitt(u16 crc, const u8 *buffer, size_t len);

static __inline u16 crc_ccitt_byte(u16 crc, const u8 c)
{
	return (crc >> 8) ^ crc_ccitt_table[(crc ^ c) & 0xff];
}

#endif /* _LINUX_CRC_CCITT_H */
